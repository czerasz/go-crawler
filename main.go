package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

func main() {
	if len(os.Args) != 2 {
		log.Fatalf("wrong amount of arguments")
	}

	url := os.Args[1]

	// Prepare request
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatalf("error preparing request: %s\n", err)
	}

	// Prepare the HTTP client
	c := http.Client{
		Timeout: 3 * time.Second,
	}

	// Do the actual request
	res, err := c.Do(req)
	if err != nil {
		log.Fatalf("error while doing HTTP request: %s\n", err)
	}

	// Read the body
	b, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatalf("error while reading the body: %s\n", err)
	}

	fmt.Printf("%s\n", string(b))
}
