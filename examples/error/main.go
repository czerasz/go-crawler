package main

import "fmt"

type erroros interface {
	String() string
}

type customError struct {
	msg    string
	status int
}

func (e customError) String() string {
	return e.msg + "!"
}

func main() {
	var err erroros
	err = customError{
		msg: "some message",
	}
	fmt.Printf("error: %s\n", err)
}
